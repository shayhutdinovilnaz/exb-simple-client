package com.exb.client.client;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-12
 */
@Controller
public class FileUploadController
{
  @Value("${service.filestorage.baseurl}")
  private String baseUrl;

  @GetMapping("/")
  public String listUploadedFiles(Model model) throws IOException
  {
    try
    {
      HttpHeaders headers = getHttpHeaders();

      String serverUrl = baseUrl + "search";
      UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverUrl).queryParam("q", "ALL");
      HttpEntity<?> entity = new HttpEntity<>(headers);

      RestTemplate restTemplate = new RestTemplate();
      HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);

      ObjectMapper mapper = new ObjectMapper();
      List<FileClientData> files = mapper.readValue(response.getBody(), new TypeReference<List<FileClientData>>()
      {
      });
      model.addAttribute("files", files);
    }
    catch (Exception e)
    {

    }

    return "uploadForm";
  }

  @PostMapping("/fileupdate")
  public String handleFileUpdate(@RequestParam("fileId") String fileId, @RequestParam("title") String title, @RequestParam(value = "tags", required = false) String tags, RedirectAttributes redirectAttributes)
  {
    HttpHeaders headers = getHttpHeaders();

    String serverUrl = baseUrl + "edit/" + fileId;
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverUrl).queryParam("title", title).queryParam("tags", tags);
    HttpEntity<?> entity = new HttpEntity<>(headers);

    RestTemplate restTemplate = new RestTemplate();
    try
    {
      HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, entity, String.class);
      if (Objects.nonNull(response.getBody()) && response.getBody().equals("1"))
      {
        redirectAttributes.addFlashAttribute("message", "You successfully update file = " + title + "!");
      }
      else
      {
        redirectAttributes.addFlashAttribute("message", "Error during update file id = " + fileId + "!");
      }
    }
    catch (Exception e)
    {
      redirectAttributes.addFlashAttribute("message", "Error during update file id = " + fileId + "!");
    }
    return "redirect:/";
  }

  @PostMapping("/filedelete")
  public String handleFileDelete(@RequestParam("fileId") String fileId, RedirectAttributes redirectAttributes)
  {
    HttpHeaders headers = getHttpHeaders();
    String serverUrl = baseUrl + "delete/" + fileId;
    UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(serverUrl);
    HttpEntity<?> entity = new HttpEntity<>(headers);

    RestTemplate restTemplate = new RestTemplate();
    try
    {
      HttpEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, entity, String.class);
      if (Objects.nonNull(response.getBody()) && response.getBody().equals("1"))
      {
        redirectAttributes.addFlashAttribute("message", "You successfully delete file = " + fileId + "!");
      }
      else
      {
        redirectAttributes.addFlashAttribute("message", "Error during delete file id = " + fileId + "!");
      }
    }
    catch (Exception e)
    {
      redirectAttributes.addFlashAttribute("message", "Error during delete file id = " + fileId + "!");
    }
    return "redirect:/";
  }

  @PostMapping("/")
  public String handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("title") String title, @RequestParam(value = "tags", required = false) String tags, RedirectAttributes redirectAttributes)
  {
    String auth = "user" + ":" + "password";
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
    String authHeader = "Basic " + new String(encodedAuth);
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    headers.add(HttpHeaders.AUTHORIZATION, authHeader);
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add("file", file.getResource());
    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

    String serverUrl = baseUrl + "upload";
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response = restTemplate.postForEntity(serverUrl, requestEntity, String.class);
    if (response.getStatusCode().is2xxSuccessful())
    {
      String fileId = response.getBody();
      serverUrl = baseUrl + "save/" + fileId;
      headers = new HttpHeaders();
      headers.add(HttpHeaders.AUTHORIZATION, authHeader);
      body = new LinkedMultiValueMap<>();
      body.add("title", title);
      body.add("tags", tags);
      restTemplate = new RestTemplate();
      requestEntity = new HttpEntity<>(body, headers);
      restTemplate.postForEntity(serverUrl, requestEntity, String.class);

      redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
    }
    else
    {
      redirectAttributes.addFlashAttribute("message", "Your file was not uploaded " + file.getOriginalFilename() + "!");
    }
    return "redirect:/";
  }

  private HttpHeaders getHttpHeaders()
  {
    String auth = "user" + ":" + "password";
    byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
    String authHeader = "Basic " + new String(encodedAuth);

    HttpHeaders headers = new HttpHeaders();
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    headers.add(HttpHeaders.AUTHORIZATION, authHeader);
    return headers;
  }

}