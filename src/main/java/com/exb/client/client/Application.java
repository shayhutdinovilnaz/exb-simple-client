package com.exb.client.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-13
 */
@SpringBootApplication
public class Application
{

  public static void main(String[] args)
  {
    SpringApplication.run(Application.class, args);
  }

}
