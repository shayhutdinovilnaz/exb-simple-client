package com.exb.client.client;

import java.util.Date;

/**
 * @author ilnaz-92@yandex.ru
 * Created on 2019-06-09
 */
public class FileClientData
{
  private String fileId;
  private String title; // can be nullable
  private String url;
  private String tags; //can be nullable
  private long size;
  private String ext; //can be nullable
  private Date date; //can be nullable
  private int type; //can be nullable

  public String getFileId()
  {
    return fileId;
  }

  public void setFileId(String fileId)
  {
    this.fileId = fileId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle(String title)
  {
    this.title = title;
  }

  public String getUrl()
  {
    return url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public String getTags()
  {
    return tags;
  }

  public void setTags(String tags)
  {
    this.tags = tags;
  }

  public long getSize()
  {
    return size;
  }

  public void setSize(long size)
  {
    this.size = size;
  }

  public String getExt()
  {
    return ext;
  }

  public void setExt(String ext)
  {
    this.ext = ext;
  }

  public Date getDate()
  {
    return date;
  }

  public void setDate(Date date)
  {
    this.date = date;
  }

  public int getType()
  {
    return type;
  }

  public void setType(int type)
  {
    this.type = type;
  }
}
